PUNTO 5
----------------------------------------

En este repositorio, se encuentra el aplicativo en Angular 10, encargado de leer y listar la ruta:

https://gorest.co.in/public/v1/users

RESULTADO FINAL

![alt text](img/captura.png "tabla")

En el siguiente link, se puede apreciar un despliegue funcional del aplicativo:

https://jntorresn.gitlab.io/consumoservicioangular/
