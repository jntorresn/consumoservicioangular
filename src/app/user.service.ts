import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseURL = 'https://gorest.co.in/public/v1/users';

  constructor(private httpClient: HttpClient) {}

  getUserList(): Observable<any> {
    return this.httpClient.get(this.baseURL);
  }
}
